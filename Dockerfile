#  FROM debian:stable-slim 
#  FROM debian:bullseye-slim 
#  FROM debian:stretch-slim 
   FROM cmch/ezmini:stretch-slim 

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    u=https://www.castlestech.com/wp-content/uploads/2016/08/201511920271676073.zip  && \
    wget -O /tmp/e1 $u --no-check-certificate && \
    unzip -p /tmp/e1 EZUSB_Linux/EZUSB_Linux_x86_64_v1.5.3.zip  > /tmp/e2 && \
    unzip -p /tmp/e2 EZUSB_Linux_x86_64_v1.5.3/driver_ezusb_v1.5.3_for_64_bit/drivers/ezusb.so > /tmp/ezusb.so && \
    unzip -p /tmp/e2 EZUSB_Linux_x86_64_v1.5.3/driver_ezusb_v1.5.3_for_64_bit/drivers/Info.plist > /tmp/Info.plist && \
    d=/usr/lib/pcsc/drivers/ezusb.bundle/Contents && \
    mkdir -p $d && \
    cp /tmp/Info.plist $d && \
    mkdir $d/Linux && \
    cp /tmp/ezusb.so $d/Linux && \
    rm -rf /tmp/*
